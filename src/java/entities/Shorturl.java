/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Javier
 */
@Entity
@Table(name = "shorturl")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Shorturl.findAll", query = "SELECT s FROM Shorturl s"),
    @NamedQuery(name = "Shorturl.findByShort1", query = "SELECT s FROM Shorturl s WHERE s.shorturlPK.short1 = :short1"),
    @NamedQuery(name = "Shorturl.findByUrl", query = "SELECT s FROM Shorturl s WHERE s.shorturlPK.url = :url")})
public class Shorturl implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ShorturlPK shorturlPK;

    public Shorturl() {
    }

    public Shorturl(ShorturlPK shorturlPK) {
        this.shorturlPK = shorturlPK;
    }

    public Shorturl(String short1, String url) {
        this.shorturlPK = new ShorturlPK(short1, url);
    }

    public ShorturlPK getShorturlPK() {
        return shorturlPK;
    }

    public void setShorturlPK(ShorturlPK shorturlPK) {
        this.shorturlPK = shorturlPK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (shorturlPK != null ? shorturlPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Shorturl)) {
            return false;
        }
        Shorturl other = (Shorturl) object;
        if ((this.shorturlPK == null && other.shorturlPK != null) || (this.shorturlPK != null && !this.shorturlPK.equals(other.shorturlPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Shorturl[ shorturlPK=" + shorturlPK + " ]";
    }
    
}
