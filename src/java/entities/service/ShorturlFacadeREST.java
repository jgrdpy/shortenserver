/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.service;

import entities.Shorturl;
import entities.ShorturlPK;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.SecureRandom;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.PathSegment;
import javax.ws.rs.core.Response;
import static javax.ws.rs.core.Response.temporaryRedirect;

/**
 *
 * @author Javier
 */
@Stateless
@Path("shortenserver")
public class ShorturlFacadeREST extends AbstractFacade<Shorturl> {
    @PersistenceContext(unitName = "ShortenServerPU")
    private EntityManager em;

    private ShorturlPK getPrimaryKey(PathSegment pathSegment) {
        /*
         * pathSemgent represents a URI path segment and any associated matrix parameters.
         * URI path part is supposed to be in form of 'somePath;short1=short1Value;url=urlValue'.
         * Here 'somePath' is a result of getPath() method invocation and
         * it is ignored in the following code.
         * Matrix parameters are used as field names to build a primary key instance.
         */
        entities.ShorturlPK key = new entities.ShorturlPK();
        javax.ws.rs.core.MultivaluedMap<String, String> map = pathSegment.getMatrixParameters();
        java.util.List<String> short1 = map.get("short1");
        if (short1 != null && !short1.isEmpty()) {
            key.setShort1(short1.get(0));
        }
        java.util.List<String> url = map.get("url");
        if (url != null && !url.isEmpty()) {
            key.setUrl(url.get(0));
        }
        return key;
    }

    public ShorturlFacadeREST() {
        super(Shorturl.class);
    }

     private SecureRandom random = new SecureRandom();

    
    @POST
    @Path("shorten/{link}")
    //@Override
    @Consumes({"application/x-www-form-urlencoded"})
    public void create(@PathParam("link") String link) {
        Shorturl entity = new Shorturl();
        ShorturlPK aux = new ShorturlPK();
        aux.setUrl(link);
        aux.setShort1(new BigInteger(130, random).toString(32));
        entity.setShorturlPK(aux);
        super.create(entity);
    }
//
//    @PUT
//    @Path("{id}")
//    @Consumes({"application/xml", "application/json"})
//    public void edit(@PathParam("id") PathSegment id, Shorturl entity) {
//        super.edit(entity);
//    }
//
//    @DELETE
//    @Path("{id}")
//    public void remove(@PathParam("id") PathSegment id) {
//        entities.ShorturlPK key = getPrimaryKey(id);
//        super.remove(super.find(key));
//    }
//
    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Response redirect(@PathParam("id") PathSegment id) throws URISyntaxException {
        entities.ShorturlPK key = getPrimaryKey(id);
        Shorturl aux = super.find(key);
        if (aux != null) {
        URI createdUri = new URI(aux.getShorturlPK().getUrl());
        return Response.temporaryRedirect(createdUri).build();
        }
        else{
            return Response.status(404).build();
        }
    }
//
//    @GET
//    @Override
//    @Produces({"application/xml", "application/json"})
//    public List<Shorturl> findAll() {
//        return super.findAll();
//    }
//
//    @GET
//    @Path("{from}/{to}")
//    @Produces({"application/xml", "application/json"})
//    public List<Shorturl> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
//        return super.findRange(new int[]{from, to});
//    }
//
//    @GET
//    @Path("count")
//    @Produces("text/plain")
//    public String countREST() {
//        return String.valueOf(super.count());
//    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
