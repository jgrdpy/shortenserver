/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Javier
 */
@Embeddable
public class ShorturlPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "Short")
    private String short1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "URL")
    private String url;

    public ShorturlPK() {
    }

    public ShorturlPK(String short1, String url) {
        this.short1 = short1;
        this.url = url;
    }

    public String getShort1() {
        return short1;
    }

    public void setShort1(String short1) {
        this.short1 = short1;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (short1 != null ? short1.hashCode() : 0);
        hash += (url != null ? url.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ShorturlPK)) {
            return false;
        }
        ShorturlPK other = (ShorturlPK) object;
        if ((this.short1 == null && other.short1 != null) || (this.short1 != null && !this.short1.equals(other.short1))) {
            return false;
        }
        if ((this.url == null && other.url != null) || (this.url != null && !this.url.equals(other.url))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.ShorturlPK[ short1=" + short1 + ", url=" + url + " ]";
    }
    
}
